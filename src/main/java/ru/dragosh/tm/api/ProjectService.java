package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Project;

import java.util.List;

public interface ProjectService {
    List<Project> findAll();
    Project find(String projectName);
    void persist(Project project);
    void merge(Project project);
    void remove(String projectId);
    void removeAll();
}