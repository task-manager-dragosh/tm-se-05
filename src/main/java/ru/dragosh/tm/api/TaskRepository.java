package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Task;

import java.util.List;

public interface TaskRepository {
    List<Task> findAll(String projectId);
    Task find(String projectId, String nameTask);
    void persist(Task task);
    void merge(Task task);
    void remove(String taskId);
    void removeAll(String projectId);
}