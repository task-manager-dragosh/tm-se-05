package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Project;

import java.util.List;

public interface ProjectRepository {
    List<Project> findAll();
    Project find(String projectName);
    void persist(Project project);
    void merge(Project project);
    void remove(String projectName);
    void removeAll();
}
