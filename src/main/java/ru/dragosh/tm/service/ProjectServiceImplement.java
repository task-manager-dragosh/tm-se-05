package ru.dragosh.tm.service;

import ru.dragosh.tm.api.ProjectRepository;
import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.entity.Project;

import java.util.List;

public class ProjectServiceImplement implements ProjectService {
    private ProjectRepository projectRepository;
    public ProjectServiceImplement(final ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project find(final String projectName) {
        return projectRepository.find(projectName);
    }

    @Override
    public void persist(final Project project) {
        projectRepository.persist(project);
    }

    @Override
    public void merge(final Project project) {
        projectRepository.merge(project);
    }

    @Override
    public void remove(final String projectId) {
        projectRepository.remove(projectId);
    }

    @Override
    public void removeAll() {
        projectRepository.removeAll();
    }
}
