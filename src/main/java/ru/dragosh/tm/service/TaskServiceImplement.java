package ru.dragosh.tm.service;

import ru.dragosh.tm.api.TaskRepository;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.MessageType;

import java.util.List;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public class TaskServiceImplement implements TaskService {
    private TaskRepository taskRepository;
    public TaskServiceImplement(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final String projectId) {
        return taskRepository.findAll(projectId);
    }

    @Override
    public Task find(final String projectId, final String nameTask) {
        return taskRepository.find(projectId, nameTask);
    }

    @Override
    public void persist(final Task task) {
        taskRepository.persist(task);
    }

    @Override
    public void merge(final Task task) {
        taskRepository.merge(task);
    }

    @Override
    public void remove(final String taskId) {
        taskRepository.remove(taskId);
    }

    @Override
    public void removeAll(final String projectId) {
        if (projectId == null || projectId.isEmpty()) {
            log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        taskRepository.removeAll(projectId);
    }
}
