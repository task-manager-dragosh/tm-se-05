package ru.dragosh.tm.command.app;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.enumeration.MessageType;
import ru.dragosh.tm.util.ConsoleUtil;

public class ExitCommand extends AbstractCommand {
    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "(после завершения ввода названия/названий введите \"exit\" + Enter)";
    }

    @Override
    public void execute() {
        ConsoleUtil.log(MessageType.PROGRAM_SHUTDOWN);
        System.exit(0);
    }
}
