package ru.dragosh.tm.command.app;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {
    public HelpCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Подробная справка по командам.";
    }

    @Override
    public void execute() {
        bootstrap.getCommands().forEach((name, command) -> {
            if (!name.equals(this.getName()) && command != this)
                System.out.println("COMMAND -> " + name + " - " + command.getDescription());
        });
    }
}
