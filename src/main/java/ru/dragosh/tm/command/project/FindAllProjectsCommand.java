package ru.dragosh.tm.command.project;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.util.ConsoleUtil;

public class FindAllProjectsCommand extends AbstractCommand {
    public FindAllProjectsCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "find all projects";
    }

    @Override
    public String getDescription() {
        return "(находит и выводит на экран все проекты из базы данных Projects)";
    }

    @Override
    public void execute() {
        projectService.findAll().forEach(ConsoleUtil::projectOutput);
    }
}