package ru.dragosh.tm.command.project;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.enumeration.MessageType;
import ru.dragosh.tm.util.ConsoleUtil;

import static ru.dragosh.tm.util.ConsoleUtil.log;
import static ru.dragosh.tm.util.ConsoleUtil.projectOutput;

public class FindProjectCommand extends AbstractCommand {
    public FindProjectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "find project";
    }

    @Override
    public String getDescription() {
        return "(находит и выводит на экран проект из базы данных Projects)";
    }

    @Override
    public void execute() {
        String projectName = readWord("Введите название проекта: ");
        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        Project project = projectService.find(projectName);
        if (project == null) {
            log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        projectOutput(project);
    }
}
