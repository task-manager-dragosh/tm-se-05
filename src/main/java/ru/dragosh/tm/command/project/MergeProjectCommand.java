package ru.dragosh.tm.command.project;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.enumeration.MessageType;
import ru.dragosh.tm.util.ConsoleUtil;

public class MergeProjectCommand extends AbstractCommand {
    public MergeProjectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "change project name";
    }

    @Override
    public String getDescription() {
        return "(изменяет название определенного проекта в базе данных Projects)";
    }

    @Override
    public void execute() {
        String projectName = readWord("Введите название проекта: ");
        String newProjectName = readWord("Введите новое название для проекта: ");
        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        Project project = projectService.find(projectName);
        if (project == null) {
            ConsoleUtil.log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        if (projectService.find(newProjectName) == null) {
            ConsoleUtil.log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        project.setName(newProjectName);
        projectService.merge(project);
    }
}