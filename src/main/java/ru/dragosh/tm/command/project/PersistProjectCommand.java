package ru.dragosh.tm.command.project;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.enumeration.MessageType;
import ru.dragosh.tm.util.ConsoleUtil;

import java.text.ParseException;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public class PersistProjectCommand extends AbstractCommand {
    public PersistProjectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "add project";
    }

    @Override
    public String getDescription() {
        return "(добавляет в Project новый проект)";
    }

    @Override
    public void execute() {
        String projectName = readWord("Введите название проекта: ");
        String projectDescription = readWord("Введите описание проекта: ");
        String projectDateStart = readWord("Введите дату начала проекта: ");
        String projectDateFinish = readWord("Введите дату окончания проекта: ");

        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (projectDescription == null || projectDescription.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (projectDateStart == null || projectDateStart.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (projectDateFinish == null || projectDateFinish.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (projectService.find(projectName) != null) {
            ConsoleUtil.log(MessageType.PROJECT_EXISTS);
            return;
        }
        Project project = null;
        try {
            project = new Project(projectName, projectDescription, projectDateStart, projectDateFinish);
        } catch (ParseException e) {
            log(MessageType.WRONG_DATE_FORMAT);
            return;
        }
        bootstrap.getProjectService().persist(project);
    }
}