package ru.dragosh.tm.command.project;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;

public class RemoveAllProjectsCommand extends AbstractCommand {
    public RemoveAllProjectsCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "remove all projects";
    }

    @Override
    public String getDescription() {
        return "(удаляет все проекты и связанные с ними задачи из баз данных Projects и Tasks)";
    }

    @Override
    public void execute() {
        projectService.removeAll();
    }
}