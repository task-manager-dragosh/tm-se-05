package ru.dragosh.tm.command.task;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.MessageType;
import ru.dragosh.tm.util.ConsoleUtil;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public class RemoveTaskCommand extends AbstractCommand {
    public RemoveTaskCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "remove task";
    }

    @Override
    public String getDescription() {
        return "(удаляет из проекта задачу)";
    }

    @Override
    public void execute() {
        String projectName = readWord("Введите название проекта: ");
        String taskName = readWord("Введите название задачи: ");

        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskName == null || taskName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        Project project = projectService.find(projectName);
        if (project == null) {
            log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        Task task = taskService.find(project.getId(), taskName);
        taskService.remove(task.getId());
    }
}