package ru.dragosh.tm.command.task;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.MessageType;
import ru.dragosh.tm.util.ConsoleUtil;

import static ru.dragosh.tm.util.ConsoleUtil.*;

public class FindTaskCommand extends AbstractCommand {
    public FindTaskCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "find task";
    }

    @Override
    public String getDescription() {
        return "(находит и выводит на экран задачу по определенному проекту из базы данных Tasks)";
    }

    @Override
    public void execute() {
        String projectName = readWord("Введите название проекта: ");
        String taskName = readWord("Введите название задачи: ");

        if (projectName == null ||  projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskName == null || taskName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        Project project = projectService.find(projectName);
        if (project == null) {
            log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        Task task = taskService.find(project.getId(), taskName);
        if (task == null) {
            log(MessageType.TASK_NOT_FOUND);
            return;
        }
        taskOutput(task);
    }
}