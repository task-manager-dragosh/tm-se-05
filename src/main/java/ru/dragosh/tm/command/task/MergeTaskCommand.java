package ru.dragosh.tm.command.task;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.MessageType;
import ru.dragosh.tm.util.ConsoleUtil;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public class MergeTaskCommand extends AbstractCommand {
    public MergeTaskCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "change task name";
    }

    @Override
    public String getDescription() {
        return "(изменяет название задачи из определенного проекта в базе данных Tasks)";
    }

    @Override
    public void execute() {
        String projectName = readWord("Введите название проекта: ");
        String taskName = readWord("Введите название задачи: ");
        String newTaskName = readWord("Введите новое название для задачи: ");

        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskName == null || taskName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (newTaskName == null || newTaskName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        Project project = projectService.find(projectName);
        if (project == null) {
            log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        Task task = taskService.find(project.getId(), taskName);
        if (task == null) {
            ConsoleUtil.log(MessageType.TASK_NOT_FOUND);
            return;
        }
        if (taskService.find(project.getId(), newTaskName) != null) {
            ConsoleUtil.log(MessageType.TASK_EXISTS);
            return;
        }
        task.setName(newTaskName);
        taskService.merge(task);
    }
}