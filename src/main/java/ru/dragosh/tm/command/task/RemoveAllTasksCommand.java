package ru.dragosh.tm.command.task;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.enumeration.MessageType;
import ru.dragosh.tm.util.ConsoleUtil;


public class RemoveAllTasksCommand extends AbstractCommand {
    public RemoveAllTasksCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "remove all tasks";
    }

    @Override
    public String getDescription() {
        return "(удаляет все задачи по определенному проекту из базы данных Tasks)";
    }

    @Override
    public void execute() {
        String projectName = readWord("Введите название проекта: ");
        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        Project project = projectService.find(projectName);
        if (project == null) {
            ConsoleUtil.log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        taskService.removeAll(project.getId());
    }
}