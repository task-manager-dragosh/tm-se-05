package ru.dragosh.tm.command.task;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.MessageType;
import ru.dragosh.tm.util.ConsoleUtil;

import java.text.ParseException;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public class PersistTaskCommand extends AbstractCommand {
    public PersistTaskCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "add task";
    }

    @Override
    public String getDescription() {
        return "(добавляет в проект новую задачу)";
    }

    @Override
    public void execute() {
        String projectName = readWord("Введите название проекта: ");
        String taskName = readWord("Введите название задачи: ");
        String taskDescription = readWord("Введите описание задачи: ");
        String taskDateStart = readWord("Введите дату начала выполнения задачи: ");
        String taskDateFinish = readWord("Введите дату окончания выполнения задачи: ");

        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskName == null || taskName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskDescription == null || taskDescription.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskDateStart == null || taskDateStart.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskDateFinish == null || taskDateFinish.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        Project project = projectService.find(projectName);
        if (project == null) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        if (taskService.find(project.getId(), taskName) != null) {
            ConsoleUtil.log(MessageType.TASK_EXISTS);
            return;
        }
        Task task = null;
        try {
            task = new Task(taskName, taskDescription, taskDateStart, taskDateFinish, project.getId());
        } catch (ParseException e) {
            log(MessageType.WRONG_DATE_FORMAT);
            return;
        }
        taskService.persist(task);
    }
}