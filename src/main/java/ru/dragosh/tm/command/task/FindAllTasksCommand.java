package ru.dragosh.tm.command.task;

import ru.dragosh.tm.bootstrap.Bootstrap;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.MessageType;
import ru.dragosh.tm.util.ConsoleUtil;

import java.util.List;

import static ru.dragosh.tm.util.ConsoleUtil.log;

public class FindAllTasksCommand extends AbstractCommand {
    public FindAllTasksCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "find all tasks";
    }

    @Override
    public String getDescription() {
        return "(находит и выводит на экран все задачи по определенному проекту из базы данных Tasks)";
    }

    @Override
    public void execute() {
        String projectName = readWord("Введите название проекта: ");
        if (projectName == null || projectName.isEmpty()) {
            ConsoleUtil.log(MessageType.WRONG_DATA_FORMAT);
            return;
        }
        Project project = projectService.find(projectName);
        if (project == null) {
            log(MessageType.PROJECT_NOT_FOUND);
            return;
        }
        List<Task> taskList = taskService.findAll(project.getId());
        taskList.forEach(ConsoleUtil::taskOutput);
    }
}