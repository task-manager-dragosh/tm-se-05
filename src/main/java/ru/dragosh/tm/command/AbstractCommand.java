package ru.dragosh.tm.command;

import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.bootstrap.Bootstrap;

import java.util.Scanner;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;
    protected ProjectService projectService;
    protected TaskService taskService;

    public AbstractCommand(final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.projectService = bootstrap.getProjectService();
        this.taskService = bootstrap.getTaskService();
    }

    public abstract String getName();
    public abstract String getDescription();
    public abstract void execute();

    public String readWord(String message) {
        System.out.print(message);
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }
}
