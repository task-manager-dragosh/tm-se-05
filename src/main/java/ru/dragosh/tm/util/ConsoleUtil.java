package ru.dragosh.tm.util;

import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.enumeration.MessageType;

import java.util.Scanner;

public class ConsoleUtil {
    public static void log(MessageType message) {
        System.out.println("MESSAGE -> " + message.getMessage());
    }

    public static void projectOutput(Project project) {
        System.out.println(MessageType.PROJECT_BORDER.getMessage());
        System.out.println(project);
        System.out.println(MessageType.END_BORDER.getMessage());
    }

    public static void taskOutput(Task task) {
        System.out.println(MessageType.TASK_BORDER.getMessage());
        System.out.println(task);
        System.out.println(MessageType.END_BORDER.getMessage());
    }

    public static String readCommand() {
        System.out.print("Введите команду -> ");
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }
}
