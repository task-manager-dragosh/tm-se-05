package ru.dragosh.tm.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Project {
    private String id = UUID.randomUUID().toString();
    private String name;
    private String description;
    private String dateStart;
    private String dateFinish;

    public Project(String name, String description, String dateStart, String dateFinish) throws ParseException{
        this.name = name;
        this.description = description;
        this.setDateStart(dateStart);
        this.setDateFinish(dateFinish);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String projectName) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        this.dateStart = dt.format(dt.parse(dateStart));
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(String dateFinish) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        this.dateFinish = dt.format(dt.parse(dateFinish));
    }

    @Override
    public String toString() {
        return "UUID проекта: " + this.id + ";\n" +
                "Название проекта: " + this.name + ";\n" +
                "Описание проекта: " + this.description + ";\n" +
                "Дата начала проекта: " + this.dateStart + ";\n" +
                "Дата окончания проекта: " + this.dateFinish + ";";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;

        if (id == null || name == null)
            return false;

        return name.equals(project.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, dateStart, dateFinish);
    }
}