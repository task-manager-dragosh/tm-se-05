package ru.dragosh.tm.bootstrap;

import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.api.TaskService;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.command.app.ExitCommand;
import ru.dragosh.tm.command.app.HelpCommand;
import ru.dragosh.tm.command.project.*;
import ru.dragosh.tm.command.task.*;
import ru.dragosh.tm.enumeration.MessageType;
import ru.dragosh.tm.repository.ProjectRepositoryImplement;
import ru.dragosh.tm.repository.TaskRepositoryImplement;
import ru.dragosh.tm.service.ProjectServiceImplement;
import ru.dragosh.tm.service.TaskServiceImplement;
import ru.dragosh.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap {
    private ProjectService projectService = new ProjectServiceImplement(new ProjectRepositoryImplement());
    private TaskService taskService = new TaskServiceImplement(new TaskRepositoryImplement());

    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init() {
        prepareCommands();
        start();
    }

    private void start() {
        ConsoleUtil.log(MessageType.WELCOME_MESSAGE);
        ConsoleUtil.log(MessageType.HELP_MESSAGE);
        while (true) {
            String stringCommand = ConsoleUtil.readCommand().toLowerCase();
            AbstractCommand command = commands.get(stringCommand);
            if (command == null)
                ConsoleUtil.log(MessageType.WRONG_COMMAND);
            else
                command.execute();
        }
    }

    public Map<String, AbstractCommand> getCommands() {
        return this.commands;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    private void prepareCommands() {
        List<AbstractCommand> commandList = new ArrayList<>();
        commandList.add(new PersistProjectCommand(this));
        commandList.add(new PersistTaskCommand(this));
        commandList.add(new FindProjectCommand(this));
        commandList.add(new FindAllProjectsCommand(this));
        commandList.add(new FindTaskCommand(this));
        commandList.add(new FindAllTasksCommand(this));
        commandList.add(new MergeProjectCommand(this));
        commandList.add(new MergeTaskCommand(this));
        commandList.add(new RemoveProjectCommand(this));
        commandList.add(new RemoveAllProjectsCommand(this));
        commandList.add(new RemoveTaskCommand(this));
        commandList.add(new RemoveAllTasksCommand(this));
        commandList.add(new HelpCommand(this));
        commandList.add(new ExitCommand(this));
        commandList.forEach(command -> commands.put(command.getName(), command));
    }
}
