package ru.dragosh.tm.repository;

import ru.dragosh.tm.api.ProjectRepository;
import ru.dragosh.tm.entity.Project;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepositoryImplement implements ProjectRepository {
    private Map<String, Project> projects = new LinkedHashMap<>();

    @Override
    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    @Override
    public Project find(final String projectName) {
        return projects.values().stream()
                .filter(project -> project.getName().equals(projectName))
                .findFirst().orElse(null);
    }

    @Override
    public void persist(final Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public void merge(final Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public void remove(final String projectId) {
        projects.remove(projectId);
    }

    @Override
    public void removeAll() {
        projects.clear();
    }
}
