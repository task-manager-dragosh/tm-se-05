package ru.dragosh.tm.repository;

import ru.dragosh.tm.api.TaskRepository;
import ru.dragosh.tm.entity.Task;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TaskRepositoryImplement implements TaskRepository {
    private Map<String, Task> tasks = new LinkedHashMap<>();

    @Override
    public List<Task> findAll(final String projectId) {
        return tasks.values().stream().filter(task -> task.getProjectId().equals(projectId)).collect(Collectors.toList());
    }

    @Override
    public Task find(final String projectId, final String taskName) {
        return tasks.values().stream()
                .filter(task -> task.getProjectId().equals(projectId) && task.getName().equals(taskName))
                .findFirst().orElse(null);
    }

    @Override
    public void persist(final Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public void merge(final Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public void remove(final String taskId) {
        tasks.remove(taskId);
    }

    @Override
    public void removeAll(final String projectId) {
        tasks.values().stream()
                .filter(task -> task.getProjectId().equals(projectId))
                .forEach(task -> tasks.remove(task.getId()));
    }
}
